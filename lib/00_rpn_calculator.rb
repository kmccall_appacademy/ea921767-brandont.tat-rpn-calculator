class RPNCalculator
  OPERATIONS = %w(+ - * /)

  def initialize
    @stack = []
  end

  def push(value)
    @stack << value
  end

  def value
    @stack.last
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def divide
    perform_operation(:/)
  end

  def times
    perform_operation(:*)
  end

  def tokens(string)
    string.split(" ").map{|c| OPERATIONS.include?(c) ? c.to_sym : c.to_i}
  end

  def evaluate(string)
    instructions = tokens(string)
    instructions.each do |value|
      if value.is_a?(Symbol)
        self.perform_operation(value)
      else
        self.push(value)
      end
    end

    self.value
  end

  def perform_operation(operation)
    raise "calculator is empty" unless @stack.length >= 2

    right_value = @stack.pop
    left_value = @stack.pop

    if operation == :+
      @stack.push(right_value + left_value)
    elsif operation == :-
      @stack.push(left_value - right_value)
    elsif operation == :*
      @stack.push(left_value * right_value)
    elsif operation == :/
      @stack.push(left_value.to_f/right_value)
    end
  end
end
